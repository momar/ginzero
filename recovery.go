package ginzero

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
)

// PanicUserMessage defines the message that will be sent to the user when a panic occured.
var PanicUserMessage = "server panicked"

// Recovery is a Gin middleware that recovers both panics and requests with errors by logging all errors to Zerolog & sending the request's errors (not including panics) to the client.
// If unset, the status code of the request will be set to 500 and the Content-Type to test/plain.
func Recovery(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			if errCast, ok := err.(error); ok {
				log.Warn().Err(errCast).Str("path", c.FullPath()).Msg("request panicked")
			} else {
				log.Warn().Interface("error", err).Str("path", c.FullPath()).Msg("request panicked")
			}
			c.Error(errors.New(PanicUserMessage))
		}
		if len(c.Errors) > 0 {
			if !c.Writer.Written() {
				c.Status(http.StatusInternalServerError)
				c.Header("Content-Type", "text/plain")
			}
			if !c.Writer.Written() || c.Writer.Size() == 0 {
				errs := make([]error, len(c.Errors))
				for i, err := range c.Errors {
					errs[i] = err
					c.Writer.WriteString(err.Error() + "\n")
				}
				log.Debug().Errs("errors", errs).Str("path", c.FullPath()).Msg("request includes error(s)")
			}
		}
	}()
	c.Next()
}
