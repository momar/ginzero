module codeberg.org/momar/ginzero

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/rs/zerolog v1.20.0
)
