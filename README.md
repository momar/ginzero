# ginzero - Zerolog for Gin

**Handles `panic` & `AbortWithError` in your Gin routes by nicely logging the errors to [Zerolog](https://github.com/rs/zerolog).**

**Important:** when using `AbortWithError`, the error message will be sent to the client!
If you don't want that, use `panic` instead, or, when using `ginzero.Must`, set the status parameter to `0`.

Usage:
```go
import (
	"codeberg.org/momar/ginzero"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(ginzero.Recovery)

	// TODO: routes & stuff
	r.GET("/fail", func(c *gin.Context) {
		_, err := ioutil.ReadFile("I-Dont-Exist")
		ginzero.Must(c, http.StatusNotFound, err)
	})

	log.Info().Msg("Webserver started at http://localhost:8080")
	log.Fatal().Err(r.Run()).Msg("Webserver has stopped")
}
```
