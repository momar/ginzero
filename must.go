package ginzero

import "github.com/gin-gonic/gin"

// Must calls c.AbortWithError(code) & then panics with nil, if err is not nil and code is positive.
// If code is non-positive and err is not nil, it panics with err.
//
// The resulting panic is intended to be caught by ginzero.Recovery, which then sends the error to the client if code is not 0.
func Must(c *gin.Context, code int, err error) {
	if err != nil {
		if code == 0 {
			panic(err)
		} else {
			c.AbortWithError(code, err)
			panic(nil)
		}
	}
}
